#!/bin/bash
if [ ! -f oldsum.sum ]; then
echo "`sudo md5sum /etc/crontab`" > oldsum.sum
fi
echo "`sudo md5sum /etc/crontab`" > actualsum.sum
DIFF=$(diff oldsum.sum actualsum.sum)
if [ "$DIFF" != "" ]
then
	echo "Your crontab file was modified!" | sudo sendmail root@debian
fi
