#!/bin/bash
sudo sed -i 's/dhcp/static/g' /etc/network/interfaces
sudo bash -c 'echo "address 10.13.42.42" >> /etc/network/interfaces'
sudo bash -c 'echo "netmask 255.255.255.252" >> /etc/network/interfaces'
sudo bash -c 'echo "broadcast 10.13.42.254" >> /etc/network/interfaces'
sudo bash -c 'echo "gateway 10.51.1.42" >> /etc/network/interfaces'
sudo bash -c 'echo "PermitRootLogin no" >> /etc/ssh/sshd_config'
sudo bash -c 'echo "Port 42" >> /etc/ssh/sshd_config'
sudo bash -c 'echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config'
sudo service networking restart
