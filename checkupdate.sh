#!/bin/bash
sudo apt-get update
sudo bash -c 'echo -n "`date +%Y-%m-%d` : " >> /var/log/update_script.log'
sudo bash -c 'apt-get upgrade | grep upgraded >> /var/log/update_script.log'
