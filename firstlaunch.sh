#!/bin/bash
echo "WARNING: Log as non-root user with sudo privileges before running this script!\n"
clear && echo "Getting necessary packagaes"
sudo apt install rcconf iptables-persistent
clear && echo "Setting up network with static ip"
sh setnetwork.sh
clear && echo "Setting up firewall"
sh setfw.sh
clear && echo "Setting up cron jobs"
sh cronmaker.sh
clear && echo "Setting up your SSL Certificate"
sh apachessl.sh
clear && echo "Putting the webpage in place"
sh getwcontent.sh
clear && echo "All done, you can do \"sudo reboot\" to see the changes!"
