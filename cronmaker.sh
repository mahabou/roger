#!/bin/bash
sudo crontab -l > mycron
sudo bash -c 'echo "0 0 * * * cronverif.sh" >> mycron'
sudo bash -c 'echo "0 4 * * * checkupdate.sh" >> mycron'
sudo bash -c 'echo "@reboot checkupdate.sh" >> mycron'
sudo crontab mycron
sudo rm mycron
